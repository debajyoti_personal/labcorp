package com.labcorp.main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Timer;

import org.apache.log4j.Logger;

import com.labcorp.connection.Connection;
import com.labcorp.logic.ScheduledTask;

public class MainClass {

	private static Logger logger = Logger.getLogger(MainClass.class.getName());

	public static Integer SCHEDULER_TIME;

	public static void main(String[] args) throws FileNotFoundException,
			IOException {

		String path = Connection.class.getProtectionDomain().getCodeSource()
				.getLocation().getPath();
		path = path.substring(0, path.lastIndexOf('/') + 1);
		path = path + "labcorp.properties";

		Properties properties = new Properties();

		properties.load(new FileInputStream(path));

		SCHEDULER_TIME = Integer.parseInt(properties
				.getProperty("scheduler.time"));

		logger.debug("Main Function Started");

		Timer time = new Timer(); // Instantiate Timer Object
		ScheduledTask st = new ScheduledTask(); // Instantiate ScheduledTask
		time.schedule(st, 0, SCHEDULER_TIME*60*1000); 

	}

}
