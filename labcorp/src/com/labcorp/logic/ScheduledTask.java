package com.labcorp.logic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.labcorp.connection.Connection;


public class ScheduledTask extends TimerTask {
	
	private static Logger logger = Logger.getLogger(ScheduledTask.class.getName());
	private static String currentPK_Lab_Order_ID = null;
	


	@Override
	public void run() {

		System.out.println("IN RUN");
		
		String received = null;
		
		try {
			received = fetchMessage();
			
			 String[] message = received.split("/");
			 
			 System.out.println("message[0]"+message[0]);
			 
			 System.out.println("message[1]"+message[1]);
			 
			 logger.debug(".................message id..............."+ message[0]);
			 logger.debug(".................ACK received........"+ message[1]);
			 
			if (message[1].contains("AA")){	
				logger.debug("........updates the message as sent as ACK is received.....");
				System.out.println("........updates the message as sent as ACK is received.....");
				updateRow(message[0]);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

		
	}
	
	public void updateRow(String id) {
				
		Connection connection = new Connection();
		java.sql.Connection con = connection.Connect();
		
		logger.debug("inside updateRow message-connection"+ con);
		
		Statement stmt = null;
		Integer i = null;
		
		try {
			
		String query = "update lab_order set sent_status = 'S' where PK_Lab_Order_ID = '"+currentPK_Lab_Order_ID+"'";
		stmt = con.createStatement();
		i = stmt.executeUpdate(query);
		
		}
		catch(Exception e){
			
			e.printStackTrace();
			try{
				con.rollback();
			}
			catch(Exception e1){}
			try{
				stmt.close();
			}
			catch(Exception e2){}
			try{
				con.close();
			}
			catch(Exception e3){}
			
		}
		finally {
			
			try{
				if(i > 0)
					con.commit();
				else
					con.rollback();
			}
			catch(Exception e1){}
			try{
				stmt.close();
			}
			catch(Exception e2){}
			try{
				con.close();
			}
			catch(Exception e3){}
			  
		}
		
		
	}
	
	
	public String fetchMessage() throws FileNotFoundException, IOException {
		
		String id = null;
		String stringToBeSent = null;
		String stringToBeSentEncrypted = null;
		
		String response= null;

		Connection connection = new Connection();
		java.sql.Connection con = connection.Connect();
		
		logger.debug("inside fetch message-connection"+ con);
		
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			
		String query = "select PK_Lab_Order_ID,string from lab_order where Lab_Comp = 'L' and sent_status = 'NS' limit 0,1";
		System.out.println("QUERY :: "+query);
		stmt = con.createStatement();
		rs = stmt.executeQuery(query);
		
		if(rs.next()) {
		
			System.out.println("String :: "+rs.getString(1));
			currentPK_Lab_Order_ID =  rs.getString(1);
			id = rs.getString(1);
			stringToBeSent = rs.getString(2);
			stringToBeSentEncrypted = stringEncryption(stringToBeSent);
		}
		
		}
		catch(Exception e){
			
			e.printStackTrace();
			try{
				rs.close();
			}
			catch(Exception e1){}
			try{
				stmt.close();
			}
			catch(Exception e2){}
			try{
				con.close();
			}
			catch(Exception e3){}
			
		}
		finally {
			//flag = 2;
			try{
				rs.close();
			}
			catch(Exception e1){}
			try{
				stmt.close();
			}
			catch(Exception e2){}
			try{
				con.close();
			}
			catch(Exception e3){}
			
			if(stringToBeSent != null) {
				
				SendData sendData= new SendData();
			    
			    response = sendData.sendHL7Data(stringToBeSentEncrypted);
			  
			}
			  
		}	
		
		return id+"/"+response;
		
		
	}
	
	private String stringEncryption(String stringToBeSent) {
		
		//System.out.println("MAHAN STRING RECEIVED FROM DB :: "+stringToBeSent);
		
		logger.debug("inside stringEncryption message-string "+ stringToBeSent);
				
		String START_OF_MESSAGE = "\u000b"; //\u000b
		String START_OF_SEGMENT[] = {"PID","NK1","ZCC","IN1","GT1","DG1","ZCI","ORC","OBR","ZBL","ZCY","ZSA","ZRE","ZON","ZAP","NTE","SPM"};
		String END_OF_MESSAGE = "\r\n\u001c\r"; //0d0a1c0d  \r\n\u001c\r
		
		stringToBeSent = START_OF_MESSAGE+stringToBeSent;
		
		final char MIDDLE_CHARACTER = 0x0d;
		
		for(String temp : START_OF_SEGMENT){
			stringToBeSent = stringToBeSent.replace(temp, MIDDLE_CHARACTER+temp);
		}
		
		stringToBeSent = stringToBeSent + END_OF_MESSAGE;

		logger.debug("inside stringEncryption encrypted-message-string "+ stringToBeSent);
		
		logger.debug("-----------------------------------------------------");
		
		
		logger.debug("inside stringEncryption encrypted-message-string hexadecimal "+ toHex(stringToBeSent));
		
		return stringToBeSent;
	}
	
	public static String toHex(String arg) {
	    return String.format("%02x", new BigInteger(1, arg.getBytes(StandardCharsets.UTF_8)));
	}
	


}
