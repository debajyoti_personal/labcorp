package com.labcorp.connection;

import java.io.FileInputStream;
import java.sql.DriverManager;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Connection {

	private static Logger logger = Logger.getLogger(Connection.class.getName());

	private java.sql.Connection con = null;

	public java.sql.Connection Connect() {

		logger.debug("Connect Started ...... ");

		try {

			String path = Connection.class.getProtectionDomain()
					.getCodeSource().getLocation().getPath();
			path = path.substring(0, path.lastIndexOf('/') + 1);
			path = path + "database.properties";

			Properties properties = new Properties();

			properties.load(new FileInputStream(path));

			String url = properties.getProperty("database.url");
			String userName = properties.getProperty("database.user");
			String password = properties.getProperty("database.password");
			String databaseDriver = properties.getProperty("database.driver");

			Class.forName(databaseDriver);

			con = DriverManager.getConnection(url, userName, password);

			con.setAutoCommit(false);

		} catch (Exception e) {

			e.printStackTrace();
		}

		finally {

			return con;
		}
	}

}
