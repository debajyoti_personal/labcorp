package com.labcorp.logic;

import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.io.*;

import com.labcorp.connection.Connection;

public class SendData {

	public static Integer SOCKET_PORT; // 9413
	public static String SERVER; // 10.48.239.75

	public String sendHL7Data(String s) throws FileNotFoundException,
			IOException {

		String path = Connection.class.getProtectionDomain().getCodeSource()
				.getLocation().getPath();
		path = path.substring(0, path.lastIndexOf('/') + 1);
		path = path + "labcorp.properties";

		Properties properties = new Properties();

		properties.load(new FileInputStream(path));

		SERVER = properties.getProperty("listener.socket.ip");
		SOCKET_PORT = Integer.parseInt(properties
				.getProperty("listener.socket.port"));

		System.out.println("SOCKET_PORT :: " + SOCKET_PORT + "  ----   "
				+ SERVER);

		OutputStream outputStream = null;
		InputStream inputStream = null;
		Socket sock = null;

		String response = null;

		try {
			sock = new Socket(SERVER, SOCKET_PORT);

			System.out.println(sock.getInetAddress());

			InputStream is = sock.getInputStream();

			System.out.println(sock.getLocalPort() + "local port");
			System.out.println(sock.getPort() + "port");
			System.out.println("Connecting..." + is);

			OutputStream os = sock.getOutputStream();

			DataOutputStream out = new DataOutputStream(os);

			// sending the string

			out.writeUTF(s);

			byte[] messageByte = new byte[1000];

			boolean end = false;

			DataInputStream in = new DataInputStream(sock.getInputStream());

			int bytesRead = in.read(messageByte);
			System.out.println(bytesRead + "  bytes read");

			if (bytesRead > -1) {
				String messageString = new String(messageByte,
						StandardCharsets.UTF_8);

				System.out.println("MESSAGE: " + messageString);
				response = messageString;
			}

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (sock != null)
				sock.close();
		}

		return response;

	}

}
